#include <Arduino.h>
#include "pinout.h"

#define TIME    500     //Big values cause resonance and noise (>5000). Use [500, 1000]
#define STEPSXY 3000*2
#define STEPSZ  1375*2

void stepper(int steps, int pin) {
  for(int x = 0; x < steps; x++) {
    digitalWrite(pin,HIGH);
    delayMicroseconds(TIME);
    digitalWrite(pin,LOW);
    delayMicroseconds(TIME);
  }
  delay(1000); // One second delay
}

void f1(){
  //CW
  digitalWrite(X_DIRECTION,HIGH); //left
  digitalWrite(Y_DIRECTION,HIGH); //front
  digitalWrite(Z_DIRECTION,HIGH); //up

  stepper(STEPSXY, X_STEP);
  stepper(STEPSXY, Y_STEP);
  stepper(STEPSZ,  Z_STEP);

  //CCW
  digitalWrite(X_DIRECTION,LOW);  //right
  digitalWrite(Y_DIRECTION,LOW);  //back
  digitalWrite(Z_DIRECTION,LOW);  //down

  stepper(STEPSXY, X_STEP);
  stepper(STEPSXY, Y_STEP);
  stepper(STEPSZ,  Z_STEP);
}

void f2(){
  digitalWrite(X_DIRECTION,HIGH); //left
  stepper(STEPSXY, X_STEP);
  digitalWrite(X_DIRECTION,LOW);  //right
  stepper(STEPSXY, X_STEP);
  
  digitalWrite(Y_DIRECTION,HIGH); //front
  stepper(STEPSXY, Y_STEP);
  digitalWrite(Y_DIRECTION,LOW);  //back
  stepper(STEPSXY, Y_STEP);
  
  digitalWrite(Z_DIRECTION,HIGH); //up
  stepper(STEPSZ,  Z_STEP);
  digitalWrite(Z_DIRECTION,LOW);  //down
  stepper(STEPSZ,  Z_STEP);
}


void setup() {
  pinMode(X_STEP,OUTPUT);
  pinMode(X_DIRECTION,OUTPUT);
  pinMode(Y_STEP,OUTPUT);
  pinMode(Y_DIRECTION,OUTPUT);
  pinMode(Z_STEP,OUTPUT);
  pinMode(Y_DIRECTION,OUTPUT);
  pinMode(STEPPERS_DISABLE,OUTPUT);

  digitalWrite(STEPPERS_DISABLE,LOW); //LOW to enable
  digitalWrite(X_DIRECTION,LOW);
  digitalWrite(Y_DIRECTION,LOW);
  digitalWrite(Y_DIRECTION,LOW);
  digitalWrite(X_STEP,LOW);
  digitalWrite(Y_STEP,LOW);
  digitalWrite(Y_STEP,LOW);
  
  delay(5000);
}

void loop() {
  f2();
}
