/* 
 * -----------------------------------------------------------------------------
 * File: pinout.h
 * Creation date: 2022-08-17
 * Modification date: 2022-08-17
 * -----------------------------------------------------------------------------
 * 
 * -----------------------------------------------------------------------------
 *                                     ARDUINO UNO
 * 
 *                                                    +-----+
 *                       +----[PWR]-------------------| USB |--+
 *                       |                            +-----+  |
 *                       |         GND/RST2  [ ][ ]            |
 *                       |       MOSI2/SCK2  [ ][ ]  A5/SCL[ ] |
 *                       |          5V/MISO2 [ ][ ]  A4/SDA[ ] |
 *                       |                             AREF[ ] |
 *                       |                              GND[ ] |
 *                       | [ ]N/C                    SCK/13[ ] |  Spindle direction
 *                       | [ ]IOREF                 MISO/12[ ] |  Spindle enable
 *                       | [ ]RST                   MOSI/11[ ]~|  Limit Z axis*
 *                       | [ ]3V3    +---+               10[ ]~|  Limit Y axis*
 *                       | [ ]5v    -| A |-               9[ ]~|  Limit X axis*
 *                       | [ ]GND   -| R |-               8[ ] |  Stepper enable/disable
 *                       | [ ]GND   -| D |-                    |
 *                       | [ ]Vin   -| U |-               7[ ] |  Direction Z axis
 *                       |          -| I |-               6[ ]~|  Direction Y axis
 *         Reset/Abort*  | [ ]A0    -| N |-               5[ ]~|  Direction X axis
 *           Feed hold*  | [ ]A1    -| O |-               4[ ] |  Step pulse Z axis
 *  Cycle start/resume*  | [ ]A2     +---+           INT1/3[ ]~|  Step pulse Y axis
 *      Coolant enable   | [ ]A3                     INT0/2[ ] |  Step pulse X axis
 *                       | [ ]A4/SDA  RST SCK MISO     TX>1[ ] |  
 *                       | [ ]A5/SCL  [ ] [ ] [ ]      RX<0[ ] |  
 *                       |            [ ] [ ] [ ]              |
 *                       |  UNO_R3    GND MOSI 5V  ____________/
 *                        \_______________________/
 *
 *                        http://busyducks.com/ascii-art-arduinos
 * 
 *            * indicates input pins, held high with internal pull-up resistors
 * -----------------------------------------------------------------------------
 */


// Define serial port pins and interrupt vectors.
#define SERIAL_RX     USART_RX_vect
#define SERIAL_UDRE   USART_UDRE_vect

// Define step pulse output pins. NOTE: All step bit pins must be on the same port.
#define STEP_DDR        DDRD
#define STEP_PORT       PORTD
#define X_STEP          2  // Uno Digital Pin 2     8mm per 200 steps => 40[nm/step]
#define Y_STEP          3  // Uno Digital Pin 3     8mm per 200 steps => 40[nm/step]
#define Z_STEP          4  // Uno Digital Pin 4     2.66mm per 200 steps => 13.33[nm/step]
#define STEP_MASK       ((1<<X_STEP    )|(1<<Y_STEP    )|(1<<Z_STEP    )) // All step bits

// Define step direction output pins. NOTE: All direction pins must be on the same port.
#define DIRECTION_DDR     DDRD
#define DIRECTION_PORT    PORTD
#define X_DIRECTION       5  // Uno Digital Pin 5
#define Y_DIRECTION       6  // Uno Digital Pin 6
#define Z_DIRECTION       7  // Uno Digital Pin 7
#define DIRECTION_MASK    ((1<<X_DIRECTION    )|(1<<Y_DIRECTION    )|(1<<Z_DIRECTION    )) // All direction bits

// Define stepper driver enable/disable output pin.
#define STEPPERS_DISABLE_DDR    DDRB
#define STEPPERS_DISABLE_PORT   PORTB
#define STEPPERS_DISABLE        8  // Uno Digital Pin 8
#define STEPPERS_DISABLE_MASK   (1<<STEPPERS_DISABLE    )
#define STEPPERS_DISABLE_PIN    8  // Uno Digital Pin 8

/*
 * Travel distance in
 * x:35mm
 * y:35mm
 * z:17mm
 */
