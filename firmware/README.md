# Firmware

## Hardware

At the moment I'm using an [Arduino UNO R3][0] with a [CNC shield v3][1] and [DRV8825 stepper motor driver carriers][2].

## grbl

I'm using the `grbl 1.1h` version [from gnea][3], specifically [the version from positron96][4] that includes a PlatformIO configuration file, but the Makefile or using the Arduino IDE are enough to compile it.

Aditionally I include a dumb test code to test the drivers and motors before uploading `grbl`.

The most important first step is to [RTFM][5], the documentation of `grbl` is really awesome and concise. The more relevant pages for me were:

* [Commands][6]
* [Configuration][7]

### Changes in grbl

The changes for this machine includes:

* Disable `VARIABLE_SPINDLE`, as stated [here][8], is needed to use the Z limit switch.
* Change travel distances, velocities and accelerations on each axis.
* Change direction of movements on neeeded axis according to Cartesian(XYZ) coordinate frame and satisfies the right-hand rule.
* Change the homing pull-off.
* Enable hard and soft limit switches, the soft limits act as a backup.

To use my version of grbl execute:
```
git clone https://github.com/positron96/grbl.git
git apply settings_patch.diff
```

#### My grbl settings

Remember that you can change the values using grbl `$` system comands, send `$$` to read the values stored in memory. I'm using `CNCjs` to send G code to grbl because `Universal Gcode Sender` is unresponsive in Fedora 36 with wayland.

I made some changes in the `defaults.h` file to have the settings as this.

```
$0=3 (Step pulse time, microseconds)
$1=255 (Step idle delay, milliseconds)
$2=0 (Step pulse invert, mask)
$3=6 (Step direction invert, mask)
$4=0 (Invert step enable pin, boolean)
$5=0 (Invert limit pins, boolean)
$6=0 (Invert probe pin, boolean)
$10=1 (Status report options, mask)
$11=0.010 (Junction deviation, millimeters)
$12=0.002 (Arc tolerance, millimeters)
$13=0 (Report in inches, boolean)
$20=1 (Soft limits enable, boolean)
$21=1 (Hard limits enable, boolean)
$22=1 (Homing cycle enable, boolean)
$23=0 (Homing direction invert, mask)
$24=25.000 (Homing locate feed rate, mm/min)
$25=1250.000 (Homing search seek rate, mm/min)
$26=25 (Homing switch debounce delay, milliseconds)
$27=2.000 (Homing switch pull-off distance, millimeters)
$30=1000 (Maximum spindle speed, RPM)
$31=0 (Minimum spindle speed, RPM)
$32=0 (Laser-mode enable, boolean)
$100=25.000 (X-axis travel resolution, step/mm)
$101=25.000 (Y-axis travel resolution, step/mm)
$102=75.000 (Z-axis travel resolution, step/mm)
$110=5000.000 (X-axis maximum rate, mm/min)
$111=5000.000 (Y-axis maximum rate, mm/min)
$112=1250.000 (Z-axis maximum rate, mm/min)
$120=50.000 (X-axis acceleration, mm/sec^2)
$121=50.000 (Y-axis acceleration, mm/sec^2)
$122=20.000 (Z-axis acceleration, mm/sec^2)
$130=343.000 (X-axis maximum travel, millimeters)
$131=343.000 (Y-axis maximum travel, millimeters)
$132=163.000 (Z-axis maximum travel, millimeters)
```

[0]: https://docs.arduino.cc/hardware/uno-rev3
[1]: https://blog.protoneer.co.nz/arduino-cnc-shield/
[2]: https://www.pololu.com/product/2133
[3]: https://github.com/gnea/grbl/wiki
[4]: https://github.com/positron96/grbl
[5]: https://en.wikipedia.org/wiki/RTFM
[6]: https://github.com/gnea/grbl/wiki/Grbl-v1.1-Commands
[7]: https://github.com/gnea/grbl/wiki/Grbl-v1.1-Configuration
[8]: https://github.com/grbl/grbl/issues/1128#issuecomment-671794559
