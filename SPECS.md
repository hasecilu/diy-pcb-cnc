# Specifications

Some relevant measures made to the real assembly and the 3D model.

## Machine dimensions

According to the assembly dimensions in the FreeCAD model:

| Dimension |   mm   |
| :-------- | :----: |
| Width     | 570.75 |
| Length    | 573.50 |
| Height    | 510.00 |

## Travel 

The max travel in each axis is presented in the next table.

| Axis | Travel [mm] | Linear rail size [mm] | ratio against rail | ratio against machine |
| :--: | :---------: | :-------------------: | :----------------: | :-------------------: |
|  X   |     342     |          500          |       68.4%        |        59.92%         |
|  Y   |     342     |          500          |       68.4%        |        59.63%         |
|  Z   |     162     |          300          |       54.0%        |        31.76%         |

### Work volume

18,948,168 [mm^3] = 18,948 [cm^3] = 18.948 [dm^3] 

That's about the same as: 
* 148 2x2x2 Rubik's cubes, 
* 97 3x3x3 Rubik's cubes, 
* 76 4x4x4 Rubik's cubes or 
* 70 5x5x5 Rubik's cubes.

## Weight

Total weight (including mototool): **11.6 [kg]**

### Weight of some moving parts

| Picture                       |         Part name          | Weight [g] |
| :---------------------------- | :------------------------: | :--------: |
| ![](images/parts/Plate04.png) |     Aluminum Plate 04      |    166     |
| ![](images/parts/Z0.png)      | Z axis without motor mount |    468     |
| ![](images/parts/Z1.png)      |    Z axis without motor    |    655     |
| ![](images/parts/Z2.png)      |      Z axis complete       |   1,397    |
| ![](images/parts/XZ.png)      |       XZ subassembly       |  3,153\*   |
| ![](images/parts/Bed_top.png) |          Bed top           |   2,715    |

\* 3,153[g] with `Plate05` made of Aluminum 6061; 2,866[g] with `Plate05` made of wood.

`Plate05` made of Aluminum and previous version made of wood.  
Made of wood: 270[g]  
Made of Aluminum 6061: 560[g]  
Delta: 290[g], +107% weight  
<img src="images/building/Al6061_vs_wood.jpg" alt="dM4" width="300"/> 
