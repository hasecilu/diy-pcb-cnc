# DIY-PCB-CNC changelog

## 1.3

Changes:

* Limit switches were added
* The BOM were updated

TODO:
* Add Z level probe
* Add jog controller

## 1.2.1

Changes:

* Plate 05 Part dimension of Z axis changed from 240[mm] to 258[mm] to match real life components.

TODO:
* Add limit switches
* Add cables

## 1.2

Changes:

* Plate 05 Part was redesigned
* The problem with Bed_full subassembly was solved
* Step files added: 
  - power supply
  - arduino & CNC shield
  - emergency button
* Colors changed to match real life

TODO:
* Add limit switches
* Add cables


## 1.1.1

All needed fasteners were added.

I think there is a problem with the `Bed_full` sub-assembly =/

## 1.1

Changes

- Z axis optimized, taking in account the mototool dimensions
- Taking in consideration more constraints
- Folder organization in assemblies
- More robust design
- Animations added
- BOM added

TODO:

- Add 3D models of electronics
- Add fasteners

## 1.0

First version completed, the design seems very weak, maybe using some wood to reinforce the structure...

