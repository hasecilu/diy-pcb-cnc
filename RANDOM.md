# Random images

Here are some random images of the making process, there are no videos =P.

## Machining?

Drilling holes in side panels for M5 screws.  
<img src="images/building/drillingSides.jpg" alt="dS" width="800"/> 

Countersinking holes for linear bearings and anti-backlash nut  of the Z axis.  
<img src="images/building/countersinking.jpg" alt="cs" width="400"/> 

Drilling 2.5[mm] holes for M3 thread to join different plates for `Plate05` part.  
<img src="images/building/drillingM3.jpg" alt="dM3" width="500"/> 

Drilling 4[mm] holes for linear bearing and block for T8 nut.  
<img src="images/building/drillingM4.jpg" alt="dM4" width="500"/> 

Tapping the M3 holes of `Plate05` part.  
<img src="images/building/tappingM3.jpg" alt="tM3" width="500"/>

Comparing finished `Plate05` parts. Aluminum 6061 vs wood. 290 grams of difference.  
<img src="images/building/Al6061_vs_wood.jpg" alt="Al6061vswood" width="500"/> 
 
## Almost complete

Testing movement in all axis before uploading `grbl`.  
<img src="images/building/test.jpg" alt="testing" width="800"/> 

Back of the machine, cable management?  
<img src="images/building/back.jpg" alt="back" width="800"/> 




## CNC progress

Progress over the time

|  0   |  1   |  2   |   3   |  4   |  5   |
| :--: | :--: | ---- | ----- | ---- | ---- |
| ![0] | ![1] | ![2] | ![3 ] | ![4] | ![5] |

[0]: images/building/build0.jpg
[1]: images/building/build1.jpg
[2]: images/building/build2.jpg
[3]: images/building/build3.jpg
[4]: images/building/build4.jpg
[5]: images/building/build5.jpg
