# Bill Of Materials

|           Doc_Label            |      Part_Label       | Pad_Length |   Shape_Length   |           Shape_Volume            | Fastener_Diameter | Fastener_Lenght | Fastener_Type | Qty. |
| ------------------------------ | --------------------- | :--------: | :--------------: | :-------------------------------: | :---------------: | :-------------: | :-----------: | :--: |
| Base                           | Base                  |     -      |        -         |                 -                 |         -         |        -        |       -       |  1   |
| 2020x450_V_slot_profile        | 2020x450mm            |    450     |        80        |   20.0 mm x 450.0 mm x 20.0 mm    |         -         |        -        |       -       |  4   |
| 2020x490_V_slot_profile        | 2020x490mm            |    490     |        80        |   20.0 mm x 490.0 mm x 20.0 mm    |         -         |        -        |       -       |  4   |
| Corner                         | Corner                |     -      |        -         |                 -                 |         -         |        -        |       -       |  8   |
| 2020_corner_bracket            | Corner bracket        |     1      | 20.5663706143592 |    21.0 mm x 21.0 mm x 17.0 mm    |         -         |        -        |       -       |  8   |
| Corner                         | M5x8-Screw            |     -      |        -         |                 -                 |        M5         |        8        |   ISO7380-1   |  8   |
| Corner                         | M5x8-Screw001         |     -      |        -         |                 -                 |        M5         |        8        |   ISO7380-1   |  8   |
| Slot_T_nuts                    | Sliding nut           |    10.5    |        31        |    5.0 mm x 10.5 mm x 10.5 mm     |         -         |        -        |       -       | 100  |
| Vertical_frame_XZ              | Assembly              |     -      |        -         |                 -                 |         -         |        -        |       -       |  1   |
| NEMA_17_with_connector         | Nema 17               |     1      | 16.9866068747319 |     1.0 mm x 11.0 mm x 8.7 mm     |         -         |        -        |       -       |  3   |
| NEMA_17_with_connector         | M2.5x25-Screw         |     -      |        -         |                 -                 |       M2.5        |       25        |    ISO7045    |  3   |
| NEMA_17_with_connector         | M2.5x25-Screw008      |     -      |        -         |                 -                 |       M2.5        |       25        |    ISO7045    |  3   |
| NEMA_17_with_connector         | M2.5x25-Screw009      |     -      |        -         |                 -                 |       M2.5        |       25        |    ISO7045    |  3   |
| NEMA_17_with_connector         | M2.5x25-Screw010      |     -      |        -         |                 -                 |       M2.5        |       25        |    ISO7045    |  3   |
| Side_wood                      | Side wood             |     9      |       1040       |   400.0 mm x 120.0 mm x 9.0 mm    |         -         |        -        |       -       |  2   |
| Plate_05                       | Plate 05              |    6.35    |       360        |   6.35 mm x 60.0 mm x 120.0 mm    |         -         |        -        |       -       |  1   |
| Plate_05                       | M3x10-Screw           |     -      |        -         |                 -                 |        M3         |       10        |    ISO4762    |  10  |
| Plate_04                       | Part                  |    6.35    |      412.4       |   76.2 mm x 6.35 mm x 130.0 mm    |         -         |        -        |       -       |  1   |
| SC8UU                          | SC8UU                 |     30     | 152.674351539646 |    22.0 mm x 30.0 mm x 34.0 mm    |         -         |        -        |       -       |  12  |
| T8_housing_bracket             | T8 housing bracket    |     16     |       128        |    30.0 mm x 30.0 mm x 34.0 mm    |         -         |        -        |       -       |  3   |
| T8_antibacklash_nut_simplified | T8 antibacklash nut   |     -      |        -         | 18.094 mm x 11.583 mm x 11.566 mm |         -         |        -        |       -       |  3   |
| CNC_mount_V2                   | Mototool mount        |     80     | 260.856322462587 |    80.0 mm x 35.0 mm x 80.0 mm    |         -         |        -        |       -       |  1   |
| Mototool                       | Mototool              |     -      |        -         |  246.0 mm x 52.276 mm x 52.5 mm   |         -         |        -        |       -       |  1   |
| Drill_bit                      | Drill bit 1.2mm       |     -      |        -         |   5.0 mm x 7.597 mm x 7.593 mm    |         -         |        -        |       -       |  1   |
| Z_axis                         | M5x20-Screw           |     -      |        -         |                 -                 |        M5         |       20        |    ISO4762    |  4   |
| Z_axis                         | M5-Nut                |     -      |        -         |                 -                 |        M5         |                 |    ISO4032    |  4   |
| Z_axis                         | M5x12-Screw           |     -      |        -         |                 -                 |        M5         |       12        |    ISO4762    |  6   |
| Z_axis                         | M4x12-Screw           |     -      |        -         |                 -                 |        M4         |       12        |   ISO10642    |  16  |
| Z_axis                         | M4x16-Screw           |     -      |        -         |                 -                 |        M4         |       16        |   ISO10642    |  4   |
| Z_axis                         | M3x8-Screw            |     -      |        -         |                 -                 |        M3         |        8        |    ISO4762    |  4   |
| SHF08                          | SHF08                 |     10     | 172.292914921463 |   24.0 mm x 10.0 mm x 42.973 mm   |         -         |        -        |       -       |  8   |
| KFL08                          | KFL08                 |    4.2     |  153.9380400259  |   26.986 mm x 48.0 mm x 7.5 mm    |         -         |        -        |       -       |  4   |
| 8mmx300_rod                    | rod_300mm             |    300     | 25.1327412287183 |   7.942 mm x 300.0 mm x 8.0 mm    |         -         |        -        |       -       |  2   |
| T8_leadscrew_350mm             | T8 Leadscrew_350      |    350     | 25.1327412287183 |    350.0 mm x 8.0 mm x 8.0 mm     |         -         |        -        |       -       |  1   |
| XZ                             | M4x20-Screw           |     -      |        -         |                 -                 |        M4         |       20        |   ISO10642    |  4   |
| XZ                             | M5x25-Screw           |     -      |        -         |                 -                 |        M5         |       25        |   ISO7380-1   |  8   |
| XZ                             | M5-Nut001             |     -      |        -         |                 -                 |        M5         |                 |    ISO4032    |  4   |
| XZ                             | M3x6-Screw            |     -      |        -         |                 -                 |        M3         |        6        |    ISO4762    |  4   |
| XZ                             | M4x16-Screw001        |     -      |        -         |                 -                 |        M4         |       16        |   ISO10642    |  4   |
| XZ                             | M4-Nut                |     -      |        -         |                 -                 |        M4         |                 |    ISO4032    |  4   |
| Steel L-Bracket for NEMA 17    | Bracket NEMA 17       |    2.6     | 58.4131112314674 |    51.0 mm x 50.0 mm x 53.0 mm    |         -         |        -        |       -       |  2   |
| Timing_pulley_GT2_60           | GT2-60                |     7      | 87.9645943005142 |    18.0 mm x 42.0 mm x 42.0 mm    |         -         |        -        |       -       |  1   |
| Timing_pulley_GT2_20           | GT2-20                |    1.27    | 65.9734457253857 |  15.77 mm x 15.984 mm x 16.0 mm   |         -         |        -        |       -       |  1   |
| Timing_belt_GT2                | GT2 band              |     6      | 407.539910921004 |   6.0 mm x 40.774 mm x 86.86 mm   |         -         |        -        |       -       |  1   |
| LSW_support_Z                  | LSW_support_Z         |     50     | 27.9999999999999 |   55.0 mm x 14.974 mm x 12.0 mm   |         -         |        -        |       -       |  2   |
| Limit_SW_PCB                   | LimitSW_CNC 1         |     -      |        -         |                 -                 |         -         |        -        |       -       |  6   |
| SW_Z                           | M3-Nut                |     -      |        -         |                 -                 |        M3         |                 |    ISO4032    |  2   |
| Beam-coupling-8mm-5mm          | Beam-coupling-8mm-5mm |     -      |        -         |  3.999 mm x 4.789 mm x 6.412 mm   |         -         |        -        |       -       |  2   |
| 8mmx500_rod                    | rod_500mm             |    500     | 25.1327412287183 |   7.942 mm x 500.0 mm x 8.0 mm    |         -         |        -        |       -       |  4   |
| T8_leadscrew_500mm             | T8 Leadscrew_500      |    500     | 25.1327412287183 |    500.0 mm x 8.0 mm x 8.0 mm     |         -         |        -        |       -       |  2   |
| Electronics                    | Electronics           |     -      |        -         |                 -                 |         -         |        -        |       -       |  1   |
| Board4electronics              | Board for electronics |     3      |       960        |   230.0 mm x 3.0 mm x 250.0 mm    |         -         |        -        |       -       |  1   |
| Arduino_CNC_shield             | Arduino CNC shield    |     -      |        -         |                 -                 |         -         |        -        |       -       |  1   |
| Arduino_CNC_shield             | M3x5.5x8-Spacer       |     -      |        -         |                 -                 |        M3         |        8        |   PCBSpacer   |  4   |
| Fuente_12V_10A                 | Power Supply          |     -      |        -         |                 -                 |         -         |        -        |       -       |  1   |
| Vertical_frame_XZ              | M3x34-Screw           |     -      |        -         |                 -                 |        M3         |      Custo      |    ISO4762    |  4   |
| Vertical_frame_XZ              | M5x14-Screw           |     -      |        -         |                 -                 |        M5         |       14        |   ISO7380-1   |  11  |
| Vertical_frame_XZ              | M5x14-Screw001        |     -      |        -         |                 -                 |        M5         |       14        |   ISO7380-1   |  11  |
| Spacer_20                      | Spacer 20mm           |     20     | 47.4380490692059 |    20.0 mm x 10.0 mm x 10.0 mm    |         -         |        -        |       -       |  4   |
| SW_X                           | M3x14-Screw           |     -      |        -         |                 -                 |        M3         |       14        |    ISO4762    |  2   |
| SW_X                           | M3x14-Screw001        |     -      |        -         |                 -                 |        M3         |       14        |    ISO4762    |  2   |
| SW_X                           | M3-Nut001             |     -      |        -         |                 -                 |        M3         |                 |    ISO4032    |  2   |
| Light                          | Light support         |     30     | 107.853981633975 |    35.0 mm x 20.0 mm x 30.0 mm    |         -         |        -        |       -       |  1   |
| Green_indicator                | AD16-22ES-ASM01_ASM   |     -      |        -         |                 -                 |         -         |        -        |       -       |  1   |
| NEMA17_support                 | Support NEMA17        |     50     | 226.071067811866 |    20.0 mm x 50.0 mm x 89.5 mm    |         -         |        -        |       -       |  1   |
| 3D-printed_corner              | 3D-printed corner     |     -      |        -         |                 -                 |         -         |        -        |       -       |  4   |
| Big_Corner                     | Big corner            |     20     | 256.568542494924 |    70.0 mm x 70.0 mm x 20.0 mm    |         -         |        -        |       -       |  4   |
| _3D_printed_corner             | M5x10-Screw           |     -      |        -         |                 -                 |        M5         |       10        |   ISO7380-1   |  24  |
| ASSembly                       | M4x8-Screw            |     -      |        -         |                 -                 |        M4         |        8        |    ISO4762    |  4   |
| Bed_full                       | Bed full              |     -      |        -         |                 -                 |         -         |        -        |       -       |  1   |
| Bed_top                        | Bed top               |     -      |        -         |                 -                 |         -         |        -        |       -       |  1   |
| Plate_03                       | Plate 03              |     9      |       1600       |   9.0 mm x 400.0 mm x 400.0 mm    |         -         |        -        |       -       |  1   |
| Spacer_6.35                    | Spacer 6.35mm         |    6.35    | 47.4380490692059 |   6.35 mm x 9.996 mm x 9.991 mm   |         -         |        -        |       -       |  20  |
| 2020x370_V_slot_profile        | 2020x370mm            |    370     |        80        |   20.0 mm x 370.0 mm x 20.0 mm    |         -         |        -        |       -       |  9   |
| Bed_top                        | M5x16-Screw           |     -      |        -         |                 -                 |        M5         |       16        |   ISO7380-1   |  18  |
| Bed_bottom                     | Bed bottom            |     -      |        -         |                 -                 |         -         |        -        |       -       |  1   |
| SK08                           | SK08                  |     14     |      149.6       |    32.8 mm x 14.0 mm x 42.0 mm    |         -         |        -        |       -       |  4   |
| KP08                           | KP08                  |     13     | 225.979837343778 |    28.5 mm x 55.0 mm x 13.0 mm    |         -         |        -        |       -       |  2   |
| LSW_support_Y                  | LSW_support_Y         |     6      |        40        |    25.5 mm x 54.0 mm x 20.0 mm    |         -         |        -        |       -       |  2   |
| Emergency_button_case          | Emergency case        |    1.5     | 102.993201165284 |  1.5 mm x 52.992 mm x 43.992 mm   |         -         |        -        |       -       |  1   |
| EmergencyButton_LAY37          | Emergency button      |     -      |        -         |                 -                 |         -         |        -        |       -       |  1   |
