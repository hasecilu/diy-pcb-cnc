# Tooling

Tools needed for machining some parts

The idea of making some DIY project is to spend the less money as posible but depending on the project some costly tools will be required and a small CNC is not the an exception.

For some people a high cost for tools can not be acceptable, so, check the table to get some idea.

|                  Tool                   |                              Usage                               | Cost  (USD)/(MXN) |
| --------------------------------------- | ---------------------------------------------------------------- | :---------------: |
| 3D printer\*                            | Printing the mototool mount, corner brackets and Y motor support |     286/5600      |
| 10 pcs kit Ball End Hex Keys\*\*        | For metric screws on literally everything                        |       5/100       |
| Universal Bench Clamp Drill Press\*\*\* | To make straight holes                                           |      30/600       |
| Drill\*\*\*                             | For making holes                                                 |      35/685       |
| Drill bits\*\*\*\*                      | For making holes                                                 |      10/200       |
| Center pointed hole punch               | To guide the drill bits                                          |       1/20        |
| Jigsaw                                  | For cutting the Aluminum plates and wood                         |      45/900       |
| M3 tap                                  | For M3 thread                                                    |       7/140       |
| M5 tap                                  | For M5 thread                                                    |       8/160       |
| T-handle tap wrench                     | For threading                                                    |      13/260       |
|                                         | Total                                                            |     440/8800      |


\* Buying the pieces is better but having a 3D printer rocks

\*\* 1.5, 2, 2.5, 3 and 4 [mm] were used

\*\*\* A drill press is preferible but I don't have one =P

\*\*\*\* 2.4[mm] for M3 thread, 3/16[in] for M4 holes, 6[mm] for M5 holes in wood sides, 5/32[in] for M5 thread, 3/8[in] for 8[mm] rods and 7/32[in] for M5 holes in metal plates were used 
