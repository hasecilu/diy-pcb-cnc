# DIY PCB CNC

Attemp of creating a small CNC using very affordable and easy to get materials (**which coincidentally I already had**).

Made just for fun, learn FreeCAD and then compare with commercial machines.

![Assembly](images/assembly.png)

## Work in progress!

* Assembly is completed and working
* As extra features I will add a Z axis probe and a jog controller later
* I could design a PCB to locate better the cables

The latest version is 1.3, is not recommended trying to build the previous versions.

### Assembly comparison
 
|                  CNC in real life                   | CAD model in FreeCAD |
| :-------------------------------------------------: | :------------------: |
| <img src="images/building/build5.jpg" width="861"/> |    ![](logo.png)     |

### Specifications

[SPECS.md](https://gitlab.com/hasecilu/diy-pcb-cnc/-/blob/master/SPECS.md)

### Used tools

[TOOLING.md](https://gitlab.com/hasecilu/diy-pcb-cnc/-/blob/master/TOOLING.md)

### Some pictures

[RANDOM.md](https://gitlab.com/hasecilu/diy-pcb-cnc/-/blob/master/RANDOM.md)

### Animations

|      CNC 3 axis movement      |     CNC spinning     |
| :---------------------------: | :------------------: |
| ![](images/CNC_animation.gif) | ![](images/spin.gif) |


## Files

The project consist on a main assembly file and several **components** files.

Every file have one `Part` object and it could have one or multiple `Body` objects. In the simplest case at least it should have one `Body` object. [Read this][0].

### Assemblies

The files are located in several folders to order the different parts and assemblies.

The main file is: `ASSembly.FCStd`. This file is made by assembling the sub-assemblies and some parts.

### Workbenches & more

The used workbenches are the following

* [FreeCAD Link Branch][1] from realthunder (actually is a version from the AUR, based on the daily version, I use ArcoLinux btw). In the future I plan to port the project to [FreeCAD 1.0][2].
* [PartDesign Workbench][3] for designing all the models.
* [Assembly4  Workbench][4] for the assembly.
* [Fasteners Workbench][5] for some screws, nuts and PCB spacers.
* [TechDraw Workbench][6] for drawing the technichal planes of the parts that need to be manufactured.
* [Path Workbench][7] is intended for generating the `gcode` for milling wood or similar but the first option to generate Gcode to mill PCBs is [FlatCAM][8].

You can generate parts via code using [Macros][9], it's something similar to [OpenSCAD][10].

A few models are from imported `step` files from grabcad.

## Background

This effort it's for have a machine with similar capabilities to some low-cost machines available in the market, due to shipping and tax costs the price increases making viable for me to design of a custom cheapy `CNC`.

The design is heavily contraint by the materials I already had laying around. I'm minimizing the number of  Aluminum plates, for some parts I'll be using wood.

Some machines available in the market:
* [CNC 3018 Pro][11]
* [DIY CNC Router 3020][12]
* [CNC1419][13]
* [CNC 1610][14]
* [CNC Wegstr][15]


## External resources

Some models are designed specifically for this project like the `FDM` parts or other that I wanted to be native FreeCAD models and not just step files and other are third-party models.

Most of the external parts come from the [FreeCAD-library](https://github.com/FreeCAD/FreeCAD-library) where sometimes I contribute some models.

<details>
<summary>Click to show/hide table with 3rd party models</summary>
  
| Resource                  | Author           |       License        |                    Link                    |
| :------------------------ | :--------------- | :------------------: | :----------------------------------------: |
| 2020 V-slot Al profile    | hasecilu         |      CC-BY 4.0       | [FreeCAD-library](https://cutt.ly/CAedZxb) |
| SK08                      | hasecilu         |      CC-BY 4.0       | [FreeCAD-library](https://cutt.ly/CAedZxb) |
| SHF08                     | hasecilu         |      CC-BY 4.0       | [FreeCAD-library](https://cutt.ly/CAedZxb) |
| KP08                      | hasecilu         |      CC-BY 4.0       | [FreeCAD-library](https://cutt.ly/CAedZxb) |
| KFL08                     | hasecilu         |      CC-BY 4.0       | [FreeCAD-library](https://cutt.ly/CAedZxb) |
| Bearing in KP08 & KFL08   | Jimmi Henry      |          ?           |     [grabcad](https://cutt.ly/BAeZGkL)     |
| Tr8x8 leadscrew           | hasecilu         |      CC-BY 4.0       | [FreeCAD-library](https://cutt.ly/CAedZxb) |
| Tr8x8 housing bracket     | hasecilu         |      CC-BY 4.0       | [FreeCAD-library](https://cutt.ly/CAedZxb) |
| Tr8x8 anti backlash nut   | hasecilu         |      CC-BY 4.0       | [FreeCAD-library](https://cutt.ly/CAedZxb) |
| T-slot sliding nut        | hasecilu         |      CC-BY 4.0       | [FreeCAD-library](https://cutt.ly/CAedZxb) |
| NEMA_17_with_connector    | BERSERK.DESIGN   | License Design Libre | [FreeCAD-library](https://cutt.ly/2AefDpE) |
| Beam-coupling-8mm-5mm     | BERSERK.DESIGN   | License Design Libre | [FreeCAD-library](https://cutt.ly/7DZtcUf) |
| Arduino Uno CNC Shield    | Anirudh Pednekar |          ?           |     [grabcad](https://cutt.ly/TF7IhEk)     |
| Power Supply              | UNIT ELECTRONICS |          ?           |     [grabcad](https://cutt.ly/EGMGesv)     |
| Emergency stop button     | WladIMirG        |          ?           | [FreeCAD-library](https://cutt.ly/kHsEXNB) |
| GT2 timing pulley (Macro) | Emilio Aguirre   |  LGPL-2.0-or-later   | [FreeCAD-Macros](https://cutt.ly/nHVxeUd)  |
| LED indicator             | bj               |          ?           |     [grabcad](https://cutt.ly/9ChNABV)     |

</details>

## Another hobby project

[lumenpnp23](https://github.com/hasecilu/lumenpnp23) a Pick & Place machine for mid-scale manufacturing mod from LumenPnP by Opulo.


[0]: https://forum.freecadweb.org/viewtopic.php?t=16730
[1]: https://github.com/realthunder/FreeCAD_assembly3
[2]: https://forum.freecadweb.org/viewtopic.php?f=10&t=68032&p=589221&hilit=developer+meeting#p589221
[3]: https://wiki.freecadweb.org/PartDesign_Workbench
[4]: https://wiki.freecadweb.org/Assembly4_Workbench
[5]: https://wiki.freecadweb.org/Fasteners_Workbench
[6]: https://wiki.freecadweb.org/TechDraw_Workbench
[7]: https://wiki.freecadweb.org/Path_Workbench
[8]: http://flatcam.org/
[9]: https://wiki.freecadweb.org/Macros
[10]: https://openscad.org/
[11]: https://es.aliexpress.com/item/1000007073911.html?algo_exp_id=2bf467a4-9d78-47a6-9a34-2d6088d731cd-1&pdp_ext_f=%7B%22sku_id%22%3A%2210000009918245428%22%7D&pdp_pi=-1%3B3731.79%3B-1%3B201646%40salePrice%3BMXN%3Bsearch-mainSearch
[12]: https://www.aliexpress.com/item/1005003465038028.html?pvid=36957ace-861c-41c6-a51e-aa0ee9901ad8&_t=gps-id:pcDetailBottomMoreOtherSeller,scm-url:1007.14452.226710.0,pvid:36957ace-861c-41c6-a51e-aa0ee9901ad8,tpp_buckets:668%232846%238114%231999&pdp_ext_f=%257B%2522sku_id%2522%253A%252212000025911811339%2522%252C%2522sceneId%2522%253A%252230050%2522%257D&pdp_pi=-1%253B13168.98%253B-1%253B-1%2540salePrice%253BMXN%253Brecommend-recommend
[13]: https://www.aliexpress.com/item/4001102473444.html?pvid=93bc39f6-e58b-4e18-8dd0-5e5210b4854f&_t=gps-id:pcDetailBottomMoreOtherSeller,scm-url:1007.40050.266918.0,pvid:93bc39f6-e58b-4e18-8dd0-5e5210b4854f,tpp_buckets:668%232846%238114%231999&pdp_ext_f=%257B%2522sku_id%2522%253A%252210000014401522433%2522%252C%2522sceneId%2522%253A%252230050%2522%257D&pdp_pi=-1%253B8047.58%253B-1%253B-1%2540salePrice%253BMXN%253Brecommend-recommend
[14]: https://www.aliexpress.com/item/4001100700430.html?pvid=596d19c7-b3de-494c-9b95-6de9874f0db6&_t=gps-id:pcDetailBottomMoreThisSeller,scm-url:1007.13339.169870.0,pvid:596d19c7-b3de-494c-9b95-6de9874f0db6,tpp_buckets:668%232846%238114%231999&pdp_ext_f=%257B%2522sku_id%2522%253A%252210000014400532126%2522%252C%2522sceneId%2522%253A%25223339%2522%257D&pdp_pi=-1%253B3616.24%253B-1%253B-1%2540salePrice%253BMXN%253Brecommend-recommend
[15]: https://wegstr.com/CNC-Wegstr/CNC-Wegstr-(English)
